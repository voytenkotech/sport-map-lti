const mongoose = require('mongoose')

const SportArea = require('./models/SportAreaModel')

mongoose.connect('mongodb://62.84.121.164/sportarea_dataset', {useNewUrlParser: true})

SportArea.create({
    id_object: '100000',
    object: 'Дворец культуры «Капотня»',
    address: '2-й квартал Капотня, дом 20А',
    id_department: '237332',
    department_name: 'Департамент культуры города Москвы',
    available_rating: '3',
    available: 'Районное',
    Coordinates: '[55.638114073790355,37.79917687563951]',
}, (error, sportarea) => {
    console.log(error, sportarea)
});