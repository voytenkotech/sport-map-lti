const http = require('http')
const express = require('express')
const app = express()
const path = require('path')
const ejs = require('ejs')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')

const f_newUserController = require('./controllers/f_newUser')
const f_storeUserController = require('./controllers/f_storeUser')

const HomeController = require('./controllers/Index')
const loginController = require('./controllers/Login')
const loginUserController = require('./controllers/loginUser')
const logoutUserController = require('./controllers/logout')
const outdoorundergroundController = require('./controllers/outdoorundergrounds')

const testpagecontroller = require('./controllers/testpage')
const expressSession = require('express-session');
const authMiddleware = require('./middleware/authMiddleware')
const flash = require('connect-flash')

const cors = require('cors')
const corsOptions = {
    origin: '*',
    credentials:  true,
    optionSeccessStatus: 200,
}
app.use(cors(corsOptions))

app.use(fileUpload())
app.use(flash());
app.use(expressSession({
    secret: 'keyboard cat'
}));
global.loggedIn = null;
app.use("*",(req,res, next) => {
    loggedIn = req.session.userId;
    next()
});

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

mongoose.connect('mongodb://localhost/sportarea_dataset', {useNewUrlParser: true})
app.set('view engine','ejs')

app.use(express.static('public'))
app.listen(3010,()=> {
    console.log('ok')
})
app.get('/', authMiddleware, HomeController)

app.get('/auth/register', f_newUserController)
app.post('/users/register', f_storeUserController)
app.get('/auth/login', loginController) 
app.post('/users/login', loginUserController)
app.get('/auth/logout', logoutUserController)
app.get('/test/testpagemap', authMiddleware, testpagecontroller)
app.get('/outdoorunderground',authMiddleware, outdoorundergroundController)