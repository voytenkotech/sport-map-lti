const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SportAreaSchema = new Schema({
    id_object: String,
    object: String,
    address: String,
    id_department: String,
    department_name: String,
    available_rating: String,
    available: String,
    Coordinates: String,
})

const SportAreas = mongoose.model('SportAreas', SportAreaSchema)

module.exports = SportAreas