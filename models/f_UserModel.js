const mongoose = require('mongoose')
const Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator')

const bcrypt = require('bcrypt')
// Схема создания пользователя.
const UserSchema = new Schema ({
    username: {
        type: String,
        required: [true,'Здесь пусто или пользователь с таким именем существует'],
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    image: String,
    password: {
        type: String,
        required: [true,'Пароль должен содержать не менее 5 символов']
    }
});

UserSchema.plugin(uniqueValidator)
//Хеширования пароля
UserSchema.pre('save', function(next){
    const user = this

    bcrypt.hash(user.password, 10, (error, hash)=> {
        user.password = hash
        next()
    })
})

// Экспорт модели
const User = mongoose.model('User', UserSchema)
module.exports = User